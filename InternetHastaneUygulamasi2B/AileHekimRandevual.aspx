﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AileHekimRandevual.aspx.cs" Inherits="InternetHastaneUygulamasi2B.AileHekimRandevual" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            text-align: center;
        }
        .auto-style2 {
            width: 100%;
        }
    </style>
</head>
<body style="background-position: 800px center; background-color: #00FFFF; background-repeat: no-repeat; background-attachment: scroll;">
    <form id="form1" runat="server">
    <div class="auto-style1" style="background-position: 800px center; background-color: #00FFFF; background-repeat: no-repeat; background-attachment: scroll;">
    
        <asp:Label ID="Label1" runat="server" Text="İl :"></asp:Label>
        <asp:DropDownList ID="DropDownList1" runat="server">
            <asp:ListItem>&lt;Seçiniz&gt;</asp:ListItem>
            <asp:ListItem>Adana</asp:ListItem>
            <asp:ListItem>Adıyaman</asp:ListItem>
            <asp:ListItem>Balıkesir</asp:ListItem>
        </asp:DropDownList>
        <br />
        <asp:Label ID="Label2" runat="server" Text="İlçe :"></asp:Label>
        <asp:DropDownList ID="DropDownList2" runat="server">
            <asp:ListItem>&lt;Seçiniz&gt;</asp:ListItem>
        </asp:DropDownList>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Sağlık Ocağı :"></asp:Label>
        <asp:DropDownList ID="DropDownList3" runat="server">
            <asp:ListItem>&lt;Seçiniz&gt;</asp:ListItem>
        </asp:DropDownList>
        <br />
        <asp:Label ID="Label4" runat="server" Text="Hekim :"></asp:Label>
        <asp:DropDownList ID="DropDownList4" runat="server">
            <asp:ListItem>&lt;Seçiniz&gt;</asp:ListItem>
        </asp:DropDownList>
        <br />
        <asp:Button ID="Button1" runat="server" Text="Temizle" OnClick="Button1_Click" />
        <asp:Button ID="Button2" runat="server" Text="Randevü Ara" />
        <br />
        <br />
        <br />
    
        <table class="auto-style2">
            <tr>
                <td>
                    <asp:ListBox ID="ListBox1" runat="server" Height="563px" Width="865px"></asp:ListBox>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
    <p style="text-align: center">
        &nbsp;</p>
</body>
</html>
